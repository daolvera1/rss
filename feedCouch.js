
var fs = require('fs'),
    FeedParser = require('feedparser'),
 	  request = require('request'),
    nano = require('nano')(process.env.npm_package_config_protocol + "://" +
              process.env.npm_package_config_url +":"+
              process.env.npm_package_config_port);


/*get rss sites from config file*/
var data = fs.readFileSync('rssFeeds.txt');

/*put rss sites into an array*/
var array = data.toString().split(',');

/*check if db exists*/
nano.db.get('news', function(err, body){

     /*create db and view if doesn't exist already*/
    if (err !== null && err.message == 'no_db_file') {
   
        nano.db.create('news', function(err, body){
            if (err) console.log("error creating db: " + err.message );
            
                console.log('----database created');
                
                 /*create view*/
                nano.use('news').insert(
                  { "views": 
                     { "author_and_title": 
                        { "map": function(doc) { emit([doc.author, doc.title], 1); } } 
                         }
                   }, '_design/dups', function (error, response) {
                         if (error) console.log('error creating view: ' + error.message);
                         else console.log("view inserted");
                });

        
        });
      }
      
});

var news = nano.use("news")
  , inserts = []
  , bulkInsert = {docs:[]}
  , index = 0 
  , feedNum = 0  /**used for bulk insert at end**/
  , dupsRemoved = 0 /**rest three used for verification**/
  , itemsRead = 0
  , itemsInserted = 0
  , rsslength = array.length;

/*function that writes RSS feeds into db and file*/
function recursGetFeeds(array){
    if (array.length > 0 ){
        var feedparser = new FeedParser(),
		    /*get a RSS URL from array*/
	      req = request(array[array.length - 1]);
        
	      req.on('error', function (error){
		        console.log( "Error: " + error.message );
			      console.log( error.stack );
	      });

		    req.on('response', function(res){
			       if ( res.statusCode != 200 )
					       console.log('bad status code');
			       res.pipe(feedparser);
        });


        feedparser.on('error', function(error){
			       console.log('Error: ' + error.message);
	  	  });

        feedparser.on('end', function(){

          
          if (++feedNum === rsslength){
             /*check for dups*/
              news.view('dups', 'author_and_title', function(err,body){
                /*iterating through every row from view*/
                for(var i = 0; i < body.rows.length;i++){
                     var row = body.rows[i];
                     /*compare each row with new feeds */
                     for (var j = bulkInsert.docs.length - 1 ; j >= 0 ; j--){
                        var doc = bulkInsert.docs[j];
                        /*remove dup if matches*/
                        if (doc.author === row.key[0] && doc.title === row.key[1]){
                              bulkInsert.docs.splice(j,1); 
                              dupsRemoved++;
                        }

                     }
                }

                 /*bulk insert the docs*/
                  news.bulk(bulkInsert, function(err, body){
                    if (err) console.log( 'there was errur: ' + err.message );
                    else {
                        console.log('bulk insertion was successful');
                        fs.writeFile('output.txt', JSON.stringify(bulkInsert,null,4), 
                          function( err, body ){
                              if (err) console.log('there was error in writeFile: ' + err.message);
                          });
                        printResults(itemsRead,body.length,dupsRemoved);
                     }
                  });
              });

                 
           }
        });

        feedparser.on('readable', function(){
             
      	    	var stream = this
      	      		, meta = this.meta
      		   	  	, item
                  , dup = false;
              itemsRead++;
		          while (item = stream.read()){
              
                        /*insert items from RSS feeds into Javascript object and elim dups*/
                       for (var i = 0;i < bulkInsert.docs.length;i++){
                          var doc = bulkInsert.docs[i];

                          if (item.author===doc.author&&item.title===doc.title&& 
                                item.datepub===doc.datepub){
                            dup = true;
                            dupsRemoved++;
                          }
                        }
                       if (!dup)
                            bulkInsert.docs[index++] = {author:item.author, title:item.title, description:item.description,
                                source:item.source.url, pubdate:item.pubdate};
		        	}
	    });
     
      array.pop();
      recursGetFeeds(array);
	}

}
function printResults(itemsRead, itemsInserted, dups){
  console.log("items read from feeds: " + itemsRead + "\nitems inserted into db: " + itemsInserted +
        "\ndups removed: " + dups);

}
recursGetFeeds(array);
